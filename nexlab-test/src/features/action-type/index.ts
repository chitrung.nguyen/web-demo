// POSTS
export interface dataPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}
export interface dataComment {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}

interface ActionPostSuccess {
  payload: dataPost[];
}

interface ActionPostFailure {
  payload: string | null;
}

interface ActionCommentSuccess {
  payload: dataComment[];
}

interface ActionCommentFailure {
  payload: string | null;
}

// USERS

export interface dataUser {
  id: number;
    name: string;
    username: string;
    email: string;
    address: {
      street:string;
      suite:string;
      city:string;
      zipcode:string;
      geo: {
        lat: string;
        lng: string;
      }
    },
    phone: string;
    website: string;
    company: {
      name: string;
      catchPhrase: string;
      bs: string;
    }
}

interface ActionUserSuccess {
  payload: dataUser[];
}

interface ActionUserFailure {
  payload: string | null;
}

interface ActionUserProfileSuccess {
  payload: dataUser;
}

interface ActionUserProfileFailure {
  payload: string | null;
}

interface ActionPostDetailSuccess {
  payload: dataPost;
}

interface ActionPostDetailFailure {
  payload: string | null;
}

export type ActionPosts = ActionPostSuccess & ActionPostFailure;
export type ActionComments = ActionCommentSuccess & ActionCommentFailure;
export type ActionUsers = ActionUserSuccess & ActionUserFailure;
export type ActionUserProfile = ActionUserProfileSuccess & ActionUserProfileFailure;
export type ActionPostDetail = ActionPostDetailSuccess & ActionPostDetailFailure;
