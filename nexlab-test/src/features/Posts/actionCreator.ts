import { message } from 'antd';
import { Dispatch } from '@reduxjs/toolkit';

import {
  fetchPosts,
  fetchPostsSuccess,
  fetchPostsFailure,
  fetchCommentsSuccess,
  fetchCommentsFailure,
  fetchComments,
  fetchPostDetail,
  fetchPostDetailSuccess,
  fetchPostDetailFailure,
} from './postsSlice';
import { addNewPostService, getListComments, getListPosts, getPostById } from '../../services/posts';

export const fetchListPosts = () => async (dispatch: Dispatch) => {
    dispatch(fetchPosts());
    try {
        const response = await getListPosts();
        const { data = [], status = '' } = response;
        if (status === 200) {
          dispatch(fetchPostsSuccess(data));
        }
    } catch (error: any) {
        dispatch(fetchPostsFailure(error));
        message.error(`${error}`);
    }
};

export const fetchListComments = () => async (dispatch: Dispatch) => {
    dispatch(fetchComments());
    try {
        const response = await getListComments();
        const { data = [], status = '' } = response;
        if (status === 200) {
          dispatch(fetchCommentsSuccess(data));
        }
    } catch (error: any) {
        dispatch(fetchCommentsFailure(error));
        message.error(`${error}`);
    }
};

export const fetchPostById =
  (id: number | undefined) => async (dispatch: Dispatch) => {
    dispatch(fetchPostDetail());
    try {
      const response = await getPostById(id);
      const { data = [], status = '' } = response;
      if (status === 200) {
        dispatch(fetchPostDetailSuccess(data));
      }
    } catch (error: any) {
      dispatch(fetchPostDetailFailure(error));
      message.error(`${error}`);
    }
  };

export const addNewPost =
  (payload: { userId: number; title: string; body: string }): any =>
  async (dispatch: Dispatch) => {
    try {
      const response = await addNewPostService(payload);
      const { data = [], status = '' } = response;
      if (status === 201) {
        message.success('Added new post successfully!')
        console.log('Added new post: ', data);
      }
      
      return response;
    } catch (error: any) {
      message.error(`${error}`);
    }
  };
