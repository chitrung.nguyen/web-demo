import { createSlice } from '@reduxjs/toolkit';
import { dataPostDetail } from '../../constants/pages/PostDetail';
import {
  ActionPosts,
  dataPost,
  ActionComments,
  ActionPostDetail,
} from '../action-type';

interface InitialState {
  listPosts: {
    loading: boolean;
    error: string | null;
    data: dataPost[];
  };
  listComments: {
    loading: boolean;
    error: string | null;
    data: any[];
  };
  postDetail: {
    loading: boolean;
    error: string | null;
    data: dataPost;
  };
}

const initialState: InitialState = {
  listPosts: {
    loading: false,
    error: null,
    data: [],
  },
  listComments: {
    loading: false,
    error: null,
    data: [],
  },
  postDetail: {
    loading: false,
    error: null,
    data: {
      ...dataPostDetail
    },
  }
};

const featurePostsSlice = createSlice({
  name: 'POSTS_REDUCER',
  initialState,
  reducers: {
    fetchPosts: (state: InitialState) => {
      state.listPosts = {
        loading: true,
        error: null,
        data: [],
      };
    },
    fetchPostsSuccess: (state: InitialState, action: ActionPosts) => {
      state.listPosts = {
        loading: false,
        error: null,
        data: action.payload,
      };
    },
    fetchPostsFailure: (state: InitialState, action: ActionPosts) => {
      state.listPosts = {
        loading: false,
        error: action.payload,
        data: [],
      };
    },
    fetchComments: (state: InitialState) => {
      state.listComments = {
        loading: true,
        error: null,
        data: [],
      };
    },
    fetchCommentsSuccess: (state: InitialState, action: ActionComments) => {
      const arr = [...action.payload];
      const groupByPostId = (objArr: any, property: any) => {
        return objArr.reduce((result: any, cur: any) => {
          if (!result[cur[property]]) {
            result[cur[property]] = [];
          }

          result[cur[property]].push(cur);

          return result;
        }, {});
      };

      const data = groupByPostId(arr, 'postId');
      state.listComments = {
        loading: false,
        error: null,
        data,
      };
    },
    fetchCommentsFailure: (state: InitialState, action: ActionComments) => {
      state.listComments = {
        loading: false,
        error: action.payload,
        data: [],
      };
    },
    fetchPostDetail: (state: InitialState) => {
      state.postDetail = {
        loading: true,
        error: null,
        data: { ...dataPostDetail },
      };
    },
    fetchPostDetailSuccess: (state: InitialState, action: ActionPostDetail) => {
      state.postDetail = {
        loading: false,
        error: null,
        data: action.payload,
      };
    },
    fetchPostDetailFailure: (state: InitialState, action: ActionPostDetail) => {
      state.postDetail = {
        loading: false,
        error: action.payload,
        data: { ...dataPostDetail },
      };
    },
  },
});

const { actions, reducer } = featurePostsSlice;
const {
  fetchPosts,
  fetchPostsSuccess,
  fetchPostsFailure,
  fetchComments,
  fetchCommentsSuccess,
  fetchCommentsFailure,
  fetchPostDetail,
  fetchPostDetailSuccess,
  fetchPostDetailFailure,
} = actions;

export {
  fetchPosts,
  fetchPostsSuccess,
  fetchPostsFailure,
  fetchComments,
  fetchCommentsSuccess,
  fetchCommentsFailure,
  fetchPostDetail,
  fetchPostDetailSuccess,
  fetchPostDetailFailure,
};
export default reducer;
