import { message } from 'antd';
import { Dispatch } from '@reduxjs/toolkit';

import {
  fetchUsers,
  fetchUsersSuccess,
  fetchUsersFailure,
  getUserProfile,
  getUserProfileSuccess,
  getUserProfileFailure,
} from './usersSlice';
import { getListUsers, getUserById } from '../../services/users';

export const fetchListUsers = () => async (dispatch: Dispatch) => {
    dispatch(fetchUsers());
    try {
        const response = await getListUsers();
        const { data = [], status = '' } = response;
        if (status === 200) {
          dispatch(fetchUsersSuccess(data));
        }
    } catch (error: any) {
        dispatch(fetchUsersFailure(error));
        message.error(`${error}`);
    }
};

export const fetchUserProfile = (id: number | undefined): any => async (dispatch: Dispatch) => {
    dispatch(getUserProfile());
    try {
        const response = await getUserById(id);
        const { data = [], status = '' } = response;
        if (status === 200) {
          dispatch(getUserProfileSuccess(data));
        }

        return response;
    } catch (error: any) {
        dispatch(getUserProfileFailure(error));
        message.error(`${error}`);
    }
};
