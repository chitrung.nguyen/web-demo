import { createSlice } from '@reduxjs/toolkit';
import { dataUserProfile } from '../../constants/pages/UserProfile';
import { ActionUsers, dataUser, ActionUserProfile } from '../action-type';

interface InitialState {
  listUsers: {
    loading: boolean;
    error: string | null;
    data: dataUser[];
  };
  userProfile: {
    loading: boolean;
    error: string | null;
    data: dataUser;
  };
}

const initialState: InitialState = {
  listUsers: {
    loading: false,
    error: null,
    data: [],
  },
  userProfile: {
    loading: false,
    error: null,
    data: {
      ...dataUserProfile
    },
  },
};

const featureUsersSlice = createSlice({
  name: 'USERS_REDUCER',
  initialState,
  reducers: {
    fetchUsers: (state: InitialState) => {
      state.listUsers = {
        loading: true,
        error: null,
        data: [],
      };
    },
    fetchUsersSuccess: (state: InitialState, action: ActionUsers) => {
      state.listUsers = {
        loading: false,
        error: null,
        data: action.payload,
      };
    },
    fetchUsersFailure: (state: InitialState, action: ActionUsers) => {
      state.listUsers = {
        loading: false,
        error: action.payload,
        data: [],
      };
    },
    getUserProfile: (state: InitialState) => {
      state.userProfile = {
        loading: true,
        error: null,
        data: {...dataUserProfile},
      };
    },
    getUserProfileSuccess: (state: InitialState, action: ActionUserProfile) => {
      state.userProfile = {
        loading: false,
        error: null,
        data: action.payload
      };
    },
    getUserProfileFailure: (state: InitialState, action: ActionUserProfile) => {
      state.userProfile = {
        loading: false,
        error: action.payload,
        data: {...dataUserProfile},
      };
    },
  },
});

const { actions, reducer } = featureUsersSlice;
const {
  fetchUsers,
  fetchUsersSuccess,
  fetchUsersFailure,
  getUserProfile,
  getUserProfileSuccess,
  getUserProfileFailure,
} = actions;

export {
  fetchUsers,
  fetchUsersSuccess,
  fetchUsersFailure,
  getUserProfile,
  getUserProfileSuccess,
  getUserProfileFailure,
};
export default reducer;
