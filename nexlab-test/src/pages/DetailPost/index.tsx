import React, { useEffect, useState } from 'react'
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypeSelector';
import { dataPostDetail } from '../../constants/pages/PostDetail';
import { dataUserProfile } from '../../constants/pages/UserProfile';
import { dataPost, dataUser } from '../../features/action-type';
import PostBox from '../../components/Post';
import LoadingWrapper from '../../components/LoadingWrapper';

import './_detailPost.scss';

interface IPostDetail {
  match: {
    params: {
      id: string;
    };
  };
  location: any
}

interface IDataPost {
  post: dataPost;
  author: dataUser | undefined;
}

const PostDetail: React.FC<IPostDetail> = ({
  match: { params: { id = '' } = {} } = {},
}) => {
  const { fetchPostById, fetchUserProfile, fetchListComments } = useActions();
  const [dataPost, setDataPost] = useState<IDataPost>({
    post: { ...dataPostDetail },
    author: { ...dataUserProfile },
  });

  const {
    postDetail: {
      loading: loadingPostDetail = false,
      data: postDetail = { ...dataPostDetail },
    } = {},
    listComments: { data: dataComments = [] } = {},
  } = useTypedSelector((state) => state.posts);
  const {
    listUsers: { loading: loadingFetchUsers = false } = {},
    userProfile: { loading: loadingUserProfile = false } = {},
  } = useTypedSelector((state) => state.users);

  useEffect(() => {
    getUserProfile();
  }, []);

  useEffect(() => {
    if (dataComments.length === 0) {
      fetchListComments();
    }
  }, [dataComments.length]);

  const getUserProfile = () => {
    fetchPostById(+id);
  };

  useEffect(() => {
    if (postDetail.userId !== 0) {
      handleDataPost();
    }
  }, [postDetail.userId]);

  const handleDataPost = () => {
    fetchUserProfile(postDetail.userId).then(
      ({ status = 0, data = { ...dataUserProfile } }) => {
        if (status === 200) {
          const dataPost = {
            post: { ...postDetail },
            author: data,
          };
          setDataPost(dataPost);
        }
      }
    );
  };

  if (loadingFetchUsers || postDetail.userId === 0 || loadingUserProfile) {
    return (
      <LoadingWrapper />
    );
  }

  return (
    <div className='post-detail'>
      {dataPost.post.id && dataPost.author?.id && (
        <PostBox
          loading={loadingPostDetail}
          postData={dataPost}
          key={postDetail.id}
          isPostDetail={true}
        />
      )}
    </div>
  );
};

export default PostDetail
