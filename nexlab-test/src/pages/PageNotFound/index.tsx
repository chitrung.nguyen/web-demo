import React from 'react'
import NotFound from '../../assets/images/404NotFound.svg';


const PageNotFound: React.FC = () => {
    return (
        <div>
            <img src={NotFound} alt='404-not-found' />
        </div>
    )
}

export default PageNotFound;
