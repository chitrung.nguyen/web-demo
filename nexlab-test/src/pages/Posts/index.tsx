import React, { useEffect, useState } from 'react';
import PostBox from '../../components/Post';
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypeSelector';
import { dataPost, dataUser } from '../../features/action-type';
import LoadingWrapper from '../../components/LoadingWrapper';

import './_posts.scss';
import { Button } from 'antd';
import AddPostModal from './AddPostModal';

interface IDataPost {
  post: dataPost;
  author: dataUser | undefined;
}

const Posts: React.FC = () => {
  const [listPost, setListPost] = useState<IDataPost[]>([]);
  const [visible, setVisible] = useState(false);
  const { fetchListPosts, fetchListUsers, fetchListComments } = useActions();
  const {
    listPosts: {
      loading: loadingFetchPosts = false,
      data: dataPosts = [],
    } = {},
  } = useTypedSelector((state) => state.posts);
  const {
    listUsers: {
      loading: loadingFetchUsers = false,
      data: dataUsers = [],
    } = {},
  } = useTypedSelector((state) => state.users);

  useEffect(() => {
    fetchListPosts();
    fetchListUsers();
    fetchListComments();
  }, []);

  useEffect(() => {
    if (dataPosts.length > 0 && dataUsers.length > 0) {
      handleDataPost();
    }
  }, [loadingFetchUsers, loadingFetchPosts]);

  const handleDataPost = () => {
    const data = dataPosts.map((post) => {
      const author = dataUsers.find((user) => post.userId === user.id);
      return {
        post: { ...post },
        author,
      };
    });
    setListPost(data);
  };

  const onClickAddPost = () => {
    setVisible(true);
  }
  const handleCancel = () => {
    setVisible(false);
  };

  if (loadingFetchPosts || loadingFetchUsers || listPost.length === 0) {
    return <LoadingWrapper />;
  }
  return (
    <div className='list-posts'>
      <Button type='primary' onClick={onClickAddPost}>
        Add new post
      </Button>
      {listPost.map((post) => (
        <PostBox
          loading={loadingFetchPosts || loadingFetchUsers}
          postData={post}
          key={post.post.id}
        />
      ))}
      <AddPostModal
        visible={visible}
        handleCancel={handleCancel}
        dataUsers={dataUsers}
      />
    </div>
  );
};

export default Posts;
