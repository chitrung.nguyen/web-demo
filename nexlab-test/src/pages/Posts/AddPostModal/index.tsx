import React, { useState } from 'react';
import { Modal, Form, Input, Button, Select } from 'antd';
import { dataUser } from '../../../features/action-type';
import { useActions } from '../../../hooks/useActions';

import './_posts.scss';

interface IAddPostModal {
  visible: boolean;
  handleCancel: () => void;
  dataUsers: dataUser[];
}

const { Option } = Select;

const AddPostModal: React.FC<IAddPostModal> = ({
  visible = false,
  handleCancel = () => {},
  dataUsers = [],
}) => {
    const { addNewPost } = useActions();
    const [loadingAddPost, setLoadingAddPost] = useState<boolean>(false);

  const handleAdd = (value: {
    userId: number;
    title: string;
    body: string;
  }) => {
    setLoadingAddPost(true);
    addNewPost(value).then(({ status = 0}) => {
        if (status === 201) {
          setLoadingAddPost(false);
          handleCancel();
        }
    });
  };

  return (
    <Modal
      title='Add new post'
      destroyOnClose
      visible={visible}
      onCancel={handleCancel}
      footer={null}
    >
      <Form
        name='basic'
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 16 }}
        onFinish={handleAdd}
      >
        <Form.Item
          label='Title'
          name='title'
          rules={[{ required: true, message: 'Please input title!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='Content'
          name='body'
          rules={[{ required: true, message: 'Please input content!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='User'
          name='userId'
          rules={[{ required: true, message: 'Please choose user!' }]}
        >
          <Select>
            {dataUsers.map((user) => (
              <Option key={user.id} value={user.id}>
                {user.name}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <div className='footer'>
          <Button onClick={handleCancel}>Cancel</Button>
          <Button type='primary' htmlType='submit' loading={loadingAddPost}>
            Submit
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default AddPostModal;
