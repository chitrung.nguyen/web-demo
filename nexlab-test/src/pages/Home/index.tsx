import React from 'react'
import Posts from '../Posts'

const HomePage: React.FC = () => {
    return (
      <div style={{ marginTop: '50px' }}>
        {/* Header component */}
        {/* Sidebar component */}
        <Posts />
        {/* Footer component */}
      </div>
    );
}

export default HomePage
