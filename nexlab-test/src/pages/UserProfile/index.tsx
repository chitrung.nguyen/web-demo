import { Col, Row } from 'antd';
import React, { useEffect } from 'react'
import { dataUserProfile } from '../../constants/pages/UserProfile';
import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypeSelector';
import LoadingWrapper from '../../components/LoadingWrapper';

import './_userProfile.scss';

interface IUserProfile {
  match: {
    params: {
      id: string;
    };
  };
}

const UserProfile: React.FC<IUserProfile> = ({
  match: { params: { id = '' } = {} } = {},
}) => {
  const { fetchUserProfile } = useActions();
  const {
    userProfile: {
      loading: loadingFetchUser = false,
      data: userProfile = {...dataUserProfile},
    } = {},
  } = useTypedSelector((state) => state.users);

  useEffect(() => {
    getUserProfile();
  }, []);

  const getUserProfile = () => {
    fetchUserProfile(+id);
  }

  if (loadingFetchUser) {
    return <LoadingWrapper />;
  }

  return (
    <>
      {userProfile && (
        <div className='profile'> 
          <h1 className='profile-title'>Author Profile</h1>
          <div className='profile-content'>
            <Row gutter={[0, 0]}>
              <Col span={4}>
                <div className='profile-label'>Name: </div>
              </Col>
              <Col span={20}>
                <div className='profile-value'>{userProfile.name}</div>
              </Col>
              <Col span={4}>
                <div className='profile-label'>Username: </div>
              </Col>
              <Col span={20}>
                <div className='profile-value'>{userProfile.username}</div>
              </Col>
              <Col span={4}>
                <div className='profile-label'>Email: </div>
              </Col>
              <Col span={20}>
                <div className='profile-value'>{userProfile.email}</div>
              </Col>
              <Col span={4}>
                <div className='profile-label'>Phone: </div>
              </Col>
              <Col span={20}>
                <div className='profile-value'>{userProfile.phone}</div>
              </Col>
            </Row>
          </div>
        </div>
      )}
    </>
  );
};

export default UserProfile
