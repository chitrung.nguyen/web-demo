import axios from 'axios';
import { isEmpty } from 'lodash';

export const request: any = async (url = '', method = '', data = {}) => {
    const config: any = {};

    if (isEmpty(data)) {
      config.headers = {
        'Content-type': 'application/json; charset=UTF-8',
      };
    }

  return await axios({
    url,
    method,
    data,
    ...config
  });
};
