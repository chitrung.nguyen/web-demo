export const HOST = 'https://jsonplaceholder.typicode.com';

export const GET_LIST_POSTS_API = `${HOST}/posts`;
export const GET_LIST_USERS_API = `${HOST}/users`;
export const GET_LIST_COMMENTS_API = `${HOST}/comments`;