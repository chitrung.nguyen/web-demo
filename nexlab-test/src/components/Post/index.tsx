import React, { useEffect, useState } from 'react';
import { Card, Divider, Skeleton, Comment, Avatar } from 'antd';
import { useHistory } from 'react-router';
import { isEmpty } from 'lodash';

import { ROUTE_POST, ROUTE_USER } from '../../routes/routes';
import { useTypedSelector } from '../../hooks/useTypeSelector';
import { dataPost, dataUser } from '../../features/action-type';
import LoadingWrapper from '../LoadingWrapper';

import './_post.scss';

interface IPostData {
  post: dataPost;
  author: dataUser | undefined;
}

interface IPostProps {
  postData: IPostData;
  loading: boolean;
  isPostDetail?: boolean;
}

const { Meta } = Card;

const PostBox: React.FC<IPostProps> = ({ postData, loading, isPostDetail = false }) => {
  const history = useHistory();
  const [postCmts, setPostCmts] = useState<any>([]);

  const { listComments: { data: dataComments = [] } = {} } = useTypedSelector(
    (state) => state.posts
  );

  useEffect(() => {
    if (!isEmpty(dataComments)) {
      renderCommentsByPost();
    }
  }, [isEmpty(dataComments)]);

  const onClickAuthor = async (id: number | undefined) => {
    history.push(`${ROUTE_USER}/${id}`);
  };

  const renderCommentsByPost = () => {
    const postId = postData.post.id;
    const postCmts = [...dataComments[postId]];
    setPostCmts(postCmts);
  };

  const handleClick = () => {
    history.push(`${ROUTE_POST}/${postData.post.id}`);
  };

  if (postCmts.length < 0) return <LoadingWrapper />;

  const renderCard = () => (
    <Card
      className='card'
      hoverable
      loading={loading}
      extra={<div onClick={handleClick}>More</div>}
    >
      <Skeleton loading={loading}>
        <Meta title={postData.post.title} />
      </Skeleton>
      <div className='content'>{postData.post.body}</div>
      <Divider />
      <div className='content-bottom'>
        <div className='comments'>{postCmts.length} comments</div>
        <div
          onClick={() => onClickAuthor(postData.author?.id)}
          className='author-name'
        >
          Author: <span>{postData.author?.name}</span>
        </div>
      </div>
    </Card>
  );

  const postComments = (comment: any) => (
    <Comment
      author={<span>{comment.email}</span>}
      avatar={
        <Avatar src='https://joeschmoe.io/api/v1/random' alt='Han Solo' />
      }
      content={
        <p className='comments'>
          {comment.body}
        </p>
      }
      datetime='a few seconds ago'
    />
  );

  const renderPostDetail = () => (
    <Card
      className='card cardDetail'
      hoverable
      loading={loading}
    >
      <Skeleton loading={loading}>
        <Meta title={postData.post.title} />
      </Skeleton>
      <div className='content'>{postData.post.body}</div>
      <Divider />
      <div className='content-bottom'>
        <div className='comments'>{postCmts.length} comments</div>
        <div
          onClick={() => onClickAuthor(postData.author?.id)}
          className='author-name'
        >
          Author: <span>{postData.author?.name}</span>
        </div>
      </div>
      <Divider />
      {postCmts.map((item: any) => (
        <div key={item.id}>{postComments(item)}</div>
      ))}
    </Card>
  );

  return (
    <>
      {isPostDetail ? renderPostDetail() : renderCard()}
    </>
  );
};

export default PostBox;
