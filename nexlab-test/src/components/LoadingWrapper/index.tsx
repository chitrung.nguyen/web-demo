import React from 'react'
import { Spin } from 'antd';

import './_loading.scss'

const LoadingWrapper = () => {
    return (
      <div className='loading'>
        <Spin />
      </div>
    );
}

export default LoadingWrapper
