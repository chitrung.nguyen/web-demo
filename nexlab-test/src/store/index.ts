import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';

import postReducer from '../features/Posts/postsSlice';
import userReducer from '../features/Users/usersSlice';

const appReducer = combineReducers({
  posts: postReducer,
  users: userReducer,
});

const rootReducer = (state: any, action: any) => appReducer(state, action);

const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default store;
