import { request } from '../configs/request';
import { GET_LIST_POSTS_API, GET_LIST_COMMENTS_API } from './../apis';

export const getListPosts = () => request(GET_LIST_POSTS_API, 'GET');
export const getListComments = () => request(GET_LIST_COMMENTS_API, 'GET');
export const getPostById = (id: number | undefined) =>
  request(`${GET_LIST_POSTS_API}/${id}`, 'GET');

export const addNewPostService = (payload: { userId: number; title: string; body: string }) =>
  request(`${GET_LIST_POSTS_API}`, 'POST', payload);