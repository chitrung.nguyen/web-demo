import { request } from '../configs/request';
import { GET_LIST_USERS_API } from '../apis';

export const getListUsers = () => request(GET_LIST_USERS_API, 'GET');
export const getUserById = (id: number | undefined) => request(`${GET_LIST_USERS_API}/${id}`, 'GET');