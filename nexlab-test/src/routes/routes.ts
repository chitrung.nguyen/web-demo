import PostsPage from "../pages/Posts";
import PageNotFound from "../pages/PageNotFound";
import HomePage from "../pages/Home";
import UserProfile from "../pages/UserProfile";
import PostDetail from "../pages/DetailPost";

const ROUTE_HOME = '/';
const ROUTE_POST = '/post';
const ROUTE_USER = '/user';

export interface IRoutes {
  path?: string;
  component: any;
  exact: boolean;
  routes?: string;
}

export const routes: IRoutes[] = [
  {
    path: ROUTE_HOME,
    component: HomePage,
    exact: true,
  },
  {
    path: ROUTE_POST,
    component: PostsPage,
    exact: true,
  },
  {
    path: `${ROUTE_POST}/:id`,
    component: PostDetail,
    exact: true,
  },
  {
    path: `${ROUTE_USER}/:id`,
    component: UserProfile,
    exact: true,
  },
  {
    component: PageNotFound,
    exact: false,
  },
];

export { ROUTE_HOME, ROUTE_POST, ROUTE_USER };
