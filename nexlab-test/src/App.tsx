import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { routes, IRoutes } from './routes/routes';

import './App.scss';

const RouteWithSubRoutes = (route: IRoutes) => {
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => <route.component {...props} routes={route.routes} />}
    />
  );
};

const App: React.FC = () => {
  return (
    <div className='App'>
      <Switch>
        {routes.map((route, index) => (
          <RouteWithSubRoutes key={`${index + 0}`} {...route} />
        ))}
      </Switch>
    </div>
  );
};

export default App;
