import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionPostCreators, actionUserCreators } from '../features';

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators({...actionPostCreators, ...actionUserCreators}, dispatch);
};
